/*

How to use?
***********

If you have Nix installed, you can get an environment with everything
needed to compile poly2tri-c by running:

    $ nix-shell

at the root of the poly2tri-c repository.

You can also compile poly2tri-c and ‘install’ it by running:

    $ nix-build

at the root of the poly2tri-c repository. The command will install
poly2tri-c to a location under Nix store and create a ‘result’ symlink
in the current directory pointing to the in-store location.

The dependencies are pinned, you can update them to latest versions with:

    $ nix-shell --run 'niv update'

How to tweak default arguments?
*******************************

Nix supports the ‘--arg’ option (see nix-build(1)) that allows you
to override the top-level arguments.

For instance, to use your local copy of Nixpkgs:

    $ nix-build --arg pkgs "import $HOME/Projects/nixpkgs {}"

Or to speed up the build by not running the test suite:

    $ nix-build --arg doCheck false

*/

{
  # Nixpkgs instance, will default to one from Niv.
  pkgs ? null,
  # Whether to run tests when building poly2tri-c using nix-build.
  doCheck ? true,
  # Whether to build poly2tri-c, or shell for developing it.
  # We do not use `lib.inNixShell` because that would also apply
  # when in a nix-shell of some package depending on this one.
  shell ? false,
} @ args:

let
  # Pinned Nix dependencies (e.g. Nixpkgs) managed by Niv.
  sources = import ./nix/sources.nix;

  # Setting pkgs to the pinned version
  # when not overridden in args.
  pkgs =
    if args.pkgs or null == null
    then
      import sources.nixpkgs {
        overlays = [];
        config = {};
      }
    else args.pkgs;

  inherit (pkgs) lib;

  # Function for building poly2tri-c or shell for developing it.
  makeDerivation =
    if shell
    then pkgs.mkShell
    else pkgs.stdenv.mkDerivation;
in
makeDerivation rec {
  name = "poly2tri-c";

  src =
    if shell
    then null
    else pkgs.nix-gitignore.gitignoreSource [] ./.;

  # Dependencies for build platform
  nativeBuildInputs = with pkgs; [
    autoconf
    automake
    libtool
    pkg-config
  ] ++ lib.optionals shell [
    niv
  ];

  # Dependencies for host platform
  buildInputs = with pkgs; [
    glib
  ];

  preConfigure = ''
    NOCONFIGURE=1 ./autogen.sh
  '';

  env = {
    NIX_CFLAGS_COMPILE = builtins.toString [
      "--std=gnu99"
      "-Wno-error"
    ];
  };

  inherit doCheck;
}
